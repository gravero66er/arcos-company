import React from "react"
import Layout from "../components/Layout"

export default function Contacts() {
  return (
    <Layout>
      <div>
        <h1>Контакты</h1>
        <p>
          Вы можете заполнить форму обратной связи и мы обязательно позвоним вам
          и ответим на любой вопрос. Кроме того, вы можете позвонить нам,
          написать на электронную почту или приехать к нам в офис в рабочее
          время.
        </p>
      </div>
    </Layout>
  )
}
