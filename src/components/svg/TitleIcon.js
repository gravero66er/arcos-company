import * as React from "react";

function TitleIcon(props) {
    return (
        <svg
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <path
                d={props.path}
                fill="#2E1C1C"
            />
        </svg>
    );
}

export default TitleIcon;